import 'package:brikoula_artisant_app/constants/colors.dart';
import 'package:flutter/material.dart';

class ReviewCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 220,
        width: MediaQuery.of(context).size.width * 0.5,
        child: Card(
          elevation: 3,
          semanticContainer: true,
          margin: EdgeInsets.all(0),
          clipBehavior: Clip.antiAliasWithSaveLayer,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Row(
                  children: [
                    CircleAvatar(
                      radius: 20,
                      backgroundImage: NetworkImage('https://upload.wikimedia.org/wikipedia/commons/d/de/Sam_Worthington_4%2C_2013.jpg'),
                    ),
                    SizedBox(width: 10,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'boufar tarek',
                          maxLines: 1,
                          style: TextStyle(
                              fontWeight: FontWeight.w700, fontSize: 15),
                        ),
                        Row(
                          children: [
                            Text('3',style: TextStyle(fontSize: 15),),
                            SizedBox(width: 5,),
                            Image.asset('assets/icons/star.png'),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                Divider(color: AppColors.primary,),
                Text(
                  'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magnaLorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna',
                  maxLines: 7,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
